@extends('layouts.app')
@section('content')
<h1>Edit your Customer info</h1>

<form action="{{action('CustomersController@update',$customers->id)}}" method = 'post'>
    @csrf
    @method('PATCH')
    <div class = "form-group">
        <label for="title"> The name of the customer you would like to change:</label>
        <input type="text" class="form-control" name = 'name' value ="{{$customers->name}}">
    </div>
    <div>
        <label for="title"> The E-mail of the user you would like to change: </label>
        <input type="email" class = "form-control" name = 'email' value = "{{$customers->email}}">
    </div>
    <div>
        <label for="title"> The phone number of the user you would like to change: </label>
        <input type="phone" class = "form-control" name = 'phone' value = "{{$customers->phone}}">
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Done!">
    </div>
</form>

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>
@endsection