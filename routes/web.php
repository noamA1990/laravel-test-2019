<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('customers', 'CustomersController')->middleware('auth');

Route::get('register2',function(){

    return view('customers.users_only');
})->name('users_only');


Route::get('customers/delete/{id}', 'CustomersController@destroy')->name('delete');
Route::get('customers/statusUpdate/{id}', 'CustomersController@statusUpdate')->name('statusUpdate');