@extends('layouts.app')
@section('content')

<a href="{{route('customers.create')}}">Add new Customer</a>
<ul>
    @foreach($customers as $customer)
        @if($customer->user_id == $current_user)
            @if($customer->status == 1)
                <li style="color:green; font-weight:bold;">
            @else
                <li style="font-weight:bold;">
            @endif
        @else
            @if($customer->status == 1)
                <li style="color:green;">
            @else
                <li>
            @endif
        @endif
                @foreach($user_name as $user)
                    @if ($user->id == $customer->user_id)
                        {{$user->name}}
                    @endif
                @endforeach
                {{$customer->name}}

                {{$customer->email}}

                {{$customer->phone}}

                <a href="{{route('customers.edit',$customer->id )}}">edit</a>

                @can('manager')
                <a method="post" href="{{route('delete',$customer->id)}}">delete</a>
                @endcan

                @cannot('manager')
                    delete
                @endcannot('manager')
                
                @can('manager')
                    @if ($customer->status== 0)
                        <a href="{{route('statusUpdate',$customer->id)}}">deal closed</a>
                    @endif
                @endcan

            </li>


@endforeach

</ul>

@endsection