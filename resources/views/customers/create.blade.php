@extends('layouts.app')
@section('content')


<form action="{{action('CustomersController@store')}}" method = 'post'>
    {{csrf_field()}}

    <div class = "form-group">
        <label for="title"> The name of the customer:</label>
        <input type="text" class="form-control" name = 'name'>
    </div>
    <div>
        <label for="title"> The E-mail of the customer: </label>
        <input type="email" class = "form-control" name = 'email'>
    </div>
    <div>
        <label for="title"> The phone of the customer: </label>
        <input type="phone" class = "form-control" name = 'phone'>
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Save">
    </div>
</form>
<br>

@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

@endsection